let headerBtn           = document.querySelector('.header-logo-btn'),
    sidebar             = document.querySelector('.sidebar'),
    main                = document.querySelector('.main'),
    searchInput         = document.querySelector('.search-input'),
    searchInputClearBtn = document.querySelector('.clear-input-btn');

// sidebar change by clicking on headerBtn 

headerBtn.addEventListener('click', function() {
    if (this.classList.contains('active')) {
        this.classList.remove('active');
        sidebar.classList.remove('folded');
        main.classList.remove('stretched');
    } else {
        this.classList.add('active');
        sidebar.classList.add('folded');
        main.classList.add('stretched');
    }
})

// if root page has 'search'

if (window.location.href.indexOf('search') !== -1) {

    // show searchInputClearBtn if something is entered into searchInput

    searchInput.addEventListener('input', function() {
        this.value !== '' ? searchInputClearBtn.style.display = 'block' : searchInputClearBtn.style.display = 'none' ;
    })

    // clear searchInput if clicked searchInputClearBtn

    searchInputClearBtn.addEventListener('click', function() {
        searchInput.value = '';
        this.style.display = 'none';
    }) 
}