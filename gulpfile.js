'use strict';
 
var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    rename     = require("gulp-rename"),
    sourcemaps = require('gulp-sourcemaps');
    // cleancss   = require('gulp-clean-css'),
    // gcmq       = require('gulp-group-css-media-queries');
    // postcss = require('gulp-postcss'),
    // sortCSSmq = require('sort-css-media-queries'),
    // cssMqpacker = require('css-mqpacker');
    // sortMediaqueries = require('css-mqpacker-sort-mediaqueries'),

sass.compiler = require('node-sass');

gulp.task('sass', function () {
  return gulp.src('markup/assets/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    // .pipe(gcmq())
    // .pipe(postcss([require("css-mqpacker")]))
    .pipe(rename({suffix: '.min'}))
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('watch', function() {
  gulp.watch('markup/assets/scss/**/*.scss', gulp.series(['sass']));
});

gulp.task('default', gulp.series(['watch', 'sass']));
